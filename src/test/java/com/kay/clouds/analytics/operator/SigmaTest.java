package com.kay.clouds.analytics.operator;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

/**
 *
 * @author Lili
 */
public class SigmaTest {

    @Test
    public void testSigma() {
        double[] data = new double[]{1.2, 3.4, 3.};
        assertThat(Sigma.standardDeviationSeries(data, 2).length).isEqualTo(2);
        assertThat(Sigma.correctedSampleStandardDeviationSeries(data, 2).length).isEqualTo(2);

        assertThat(Sigma.standardDeviationSeries(data, 3).length).isEqualTo(1);
        assertThat(Sigma.correctedSampleStandardDeviationSeries(data, 3).length).isEqualTo(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSigmaCorrectedLowerBound() {
        double[] data = new double[]{1.2, 3.4, 3.};
        assertThat(Sigma.correctedSampleStandardDeviationSeries(data, 1).length).isEqualTo(1);
    }

    @Test
    public void testSigmaNormalLowerBound() {
        double[] data = new double[]{1.2, 3.4, 3.};
        assertThat(Sigma.standardDeviationSeries(data, 1).length).isEqualTo(3);
    }

    @Test
    public void testSigmaResult() {
        double[] data = new double[]{1., 2., 3.};
        assertThat(Sigma.standardDeviationSeries(data, 3)[0]).isEqualTo(
                Math.sqrt((1. * 1. + 2. * 2. + 3. * 3.) / 3. - ((1. + 2. + 3.) / 3.) * ((1. + 2. + 3.) / 3.))
        );

        assertThat(Sigma.standardDeviationSeries(data, 2)[0]).isEqualTo(
                Math.sqrt((1. * 1. + 2. * 2.) / 2. - ((1. + 2.) / 2.) * ((1. + 2.) / 2.))
        );

        assertThat(Sigma.standardDeviationSeries(data, 2)[1]).isEqualTo(
                Math.sqrt((3. * 3. + 2. * 2.) / 2. - ((3. + 2.) / 2.) * ((3. + 2.) / 2.))
        );

        assertThat(Sigma.correctedSampleStandardDeviationSeries(data, 3)[0]).isEqualTo(
                Math.sqrt((1. * 1. + 2. * 2. + 3. * 3.) / 2. - ((1. + 2. + 3.) / 3.) * ((1. + 2. + 3.) / 2.))
        );

        assertThat(Sigma.correctedSampleStandardDeviationSeries(data, 2)[0]).isEqualTo(
                Math.sqrt((1. * 1. + 2. * 2.) / 1. - ((1. + 2.) / 2.) * ((1. + 2.) / 1.))
        );

        assertThat(Sigma.correctedSampleStandardDeviationSeries(data, 2)[1]).isEqualTo(
                Math.sqrt((3. * 3. + 2. * 2.) / 1. - ((3. + 2.) / 2.) * ((3. + 2.) / 1.))
        );

    }
}
