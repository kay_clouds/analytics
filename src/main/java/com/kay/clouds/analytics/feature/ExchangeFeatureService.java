package com.kay.clouds.analytics.feature;

import com.kay.clouds.analytics.algorithm.Algorithm;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import static com.kay.clouds.analytics.algorithm.Algorithm.ONE_YEAR_IN_MILLI;
import com.kay.clouds.domain.analytics.Feature;
import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.StockExchangeRepository;
import com.kay.clouds.domain.analytics.feature.FeatureService;
import com.kay.clouds.domain.analytics.feature.NumberResult;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Lili
 */
public class ExchangeFeatureService implements FeatureService {

    private final StockExchangeRepository dataRepository;
    private final Map<Feature, Algorithm> algorithmMap;

    public ExchangeFeatureService(StockExchangeRepository stockExchangeRepository) {
        algorithmMap = Maps.newHashMap();
        algorithmMap.put(Feature.Volatility, Algorithm.VOLATILITY);
        this.dataRepository = stockExchangeRepository;
    }

    @Override
    public List<NumberResult> calculate(
            Feature feature,
            List<String> tickerList,
            Date initialDate,
            Date finalDate
    ) {

        Date oneYearAgo = new Date(initialDate.getTime() - ONE_YEAR_IN_MILLI);
        Algorithm algorithm = algorithmMap.get(feature);
        List<NumberResult> resultList = Lists.newArrayList();
        for (String ticker : tickerList) {

            List<? extends Exchange> dataList = dataRepository
                    .getAscendantListBetween(ticker, oneYearAgo, finalDate);
            Date intial;
            if (dataList.isEmpty()) {
                continue;
            } else {

                Date lastDataDate = dataList.get(dataList.size() - 1).getDate();
                intial = initialDate.before(lastDataDate) ? initialDate : lastDataDate;
            }

            SimpleResult result = new SimpleResult();
            result.setTicker(ticker);
            result.setDataList(algorithm.process(dataList, intial));
            resultList.add(result);
        }
        return resultList;
    }
}
