package com.kay.clouds.analytics.feature;

import com.kay.clouds.domain.analytics.feature.NumberResult;
import com.kay.clouds.domain.analytics.feature.NumberResult.Data;
import java.util.Date;
import java.util.List;

/**
 * A simple result implements Number result.
 *
 * @see NumberResult
 * @author Lili
 * @param <T>
 */
public class SimpleResult<T extends Number> implements NumberResult<T> {

    /**
     * A simple data implements data.
     *
     * @see Data
     * @author Lili
     * @param <T>
     */
    public static class SimpleData<T> implements Data<T> {

        private Date date;
        private T value;

        @Override
        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        @Override
        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

    }

    private List<Data<T>> dataList;
    private String ticker;

    @Override
    public List<Data<T>> getDataList() {
        return dataList;
    }

    public void setDataList(List<Data<T>> dataList) {
        this.dataList = dataList;
    }

    @Override
    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

}
