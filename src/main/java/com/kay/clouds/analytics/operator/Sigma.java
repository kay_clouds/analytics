package com.kay.clouds.analytics.operator;

/**
 *
 * @author Lili
 */
public class Sigma {

    private static double moveSum(
            double headerDecrease,
            double tailIncrease,
            double sumToMove
    ) {
        sumToMove -= headerDecrease;
        sumToMove += tailIncrease;
        return sumToMove;
    }

    private static double moveSquareSum(
            double headerDecrease,
            double tailIncrease,
            double sumSquareToMove
    ) {
        sumSquareToMove -= headerDecrease * headerDecrease;
        sumSquareToMove += tailIncrease * tailIncrease;
        return sumSquareToMove;
    }

    private static double sigma(
            double sum,
            double sumSquare,
            double range
    ) {

        double mean = sum / range;
        //get sigma = sqrt( sum(x^2)/n-x_mean^2)
        return Math.sqrt(sumSquare / range - mean * mean);
    }

    private static double correctedSigma(
            double sum,
            double sumSquare,
            double range,
            double sampleCorrectedFactor// should be range -1

    ) {

        //get sigma = sqrt( sum(x^2)/(n-1) -x_mean^2*n/(n-1))
        double meanSquare = sum / range * sum / (sampleCorrectedFactor);
        return Math.sqrt(sumSquare / sampleCorrectedFactor - meanSquare);
    }

    public static double[] standardDeviationSeries(
            double[] data,
            int dataRange
    ) {

        double[] result = new double[data.length - dataRange + 1];
        double sum = 0.0;
        double sumSquare = 0.0;
        int seriesPosition;

        // init data sum and sum square
        for (int position = 0; position < dataRange; position++) {
            sum += data[position];
            sumSquare += data[position] * data[position];
        }

        for (int dataPosition = dataRange; dataPosition < data.length; dataPosition++) {

            //get result position
            seriesPosition = dataPosition - dataRange;
            result[seriesPosition] = sigma(sum, sumSquare, dataRange);

            // move sum to the next
            sum = moveSum(
                    data[seriesPosition],// header to remove
                    data[dataPosition],// tail to add
                    sum//sum to change
            );
            // move square to the next
            sumSquare = moveSquareSum(
                    data[seriesPosition], //header to remove
                    data[dataPosition], //tail to add
                    sumSquare // sum square to change
            );

        }

        // last result
        result[result.length - 1] = sigma(sum, sumSquare, dataRange);

        return result;

    }

    public static double[] correctedSampleStandardDeviationSeries(
            double[] data,
            int dataRange
    ) {

        Numbers.isGreater(dataRange, 1, "DATARANGE_SHOULD_GREATER_THAN_ONE");

        double[] result = new double[data.length - dataRange + 1];
        double sum = 0.0;
        double sumSquare = 0.0;
        double sampleCorrectedFactor = dataRange - 1;
        int seriesPosition;

        // init data sum
        for (int position = 0; position < dataRange; position++) {
            sum += data[position];
            sumSquare += data[position] * data[position];
        }

        for (int dataPosition = dataRange; dataPosition < data.length; dataPosition++) {

            seriesPosition = dataPosition - dataRange;
            //  get corrected sigma
            result[seriesPosition] = correctedSigma(
                    sum,
                    sumSquare,
                    dataRange,
                    sampleCorrectedFactor
            );

            // move sum to the next
            sum = moveSum(
                    data[seriesPosition],// header to remove
                    data[dataPosition],// tail to add
                    sum//sum to change
            );
            // move square to the next
            sumSquare = moveSquareSum(
                    data[seriesPosition], //header to remove
                    data[dataPosition], //tail to add
                    sumSquare // sum square to change
            );

        }
        //get lasted result
        result[result.length - 1] = correctedSigma(
                sum,
                sumSquare,
                dataRange,
                sampleCorrectedFactor
        );

        return result;

    }
}
