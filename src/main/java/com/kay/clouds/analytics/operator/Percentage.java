package com.kay.clouds.analytics.operator;

/**
 *
 * @author Lili
 */
public class Percentage {

    public static double differenceInPercentage(double from, double to) {
        return (from - to) / from;
    }
}
