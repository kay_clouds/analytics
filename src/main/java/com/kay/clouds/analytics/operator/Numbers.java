package com.kay.clouds.analytics.operator;

/**
 *
 * @author Lili
 */
public class Numbers {

    public static void isSame(int from, int to, String text) {
        if (from != to) {
            throw new IllegalArgumentException(text);
        }
    }

    public static void isGreater(int from, int to, String text) {
        if (from <= to) {
            throw new IllegalArgumentException(text);
        }
    }
}
