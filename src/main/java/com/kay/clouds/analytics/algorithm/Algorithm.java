package com.kay.clouds.analytics.algorithm;

import com.google.common.collect.Lists;
import com.kay.clouds.analytics.feature.SimpleResult.SimpleData;
import com.kay.clouds.analytics.operator.Percentage;
import com.kay.clouds.analytics.operator.Sigma;
import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.feature.NumberResult.Data;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author lili
 * @param <T>
 */
@FunctionalInterface
public interface Algorithm<T extends Number> {

    List<Data> process(List<? extends Exchange> dataList, Date seriesInitialDate);

    static final double YEAR_PERIOD = Math.sqrt(260.0);
    static final long ONE_YEAR_IN_MILLI = TimeUnit.DAYS.toMillis(365);

    static final Algorithm<Double> VOLATILITY = (dataList, intial) -> {

        List<Date> resultDate = Lists.newArrayList();

        double[] dataSet = new double[dataList.size() - 1];
        int dataRange = 0;
        for (int index = 1; index < dataList.size(); index++) {
            Exchange data = dataList.get(index);
            Exchange dataFrom = dataList.get(index - 1);
            dataSet[index - 1] = Percentage.differenceInPercentage(
                    dataFrom.getClosingPrice(), data.getClosingPrice()
            );

            if (data.getDate().before(intial)) {
                dataRange++;
            } else {
                resultDate.add(data.getDate());
            }

        }

        List<Data> result = Lists.newArrayList();

        //data range include first elment
        double[] resultSet = Sigma.correctedSampleStandardDeviationSeries(dataSet, dataRange + 1);

        for (int index = 0; index < resultSet.length; index++) {
            SimpleData data = new SimpleData();
            data.setValue(resultSet[index] * YEAR_PERIOD);
            data.setDate(resultDate.get(index));
            result.add(data);
        }

        return result;
    };
}
