/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.exchange.yahoo;

import com.kay.clouds.analytics.crawler.Output;
import com.kay.clouds.domain.analytics.exchange.ExchangeRepository;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Lili
 */
public class ExchangeOutput implements Output {

    private List<String> dataValueSequence;
    private final ExchangeRepository exchangeRepository;

    public static ExchangeOutput newOutput(
            ExchangeRepository exchangeRepository
    ) {
        return new ExchangeOutput(
                exchangeRepository
        );
    }

    public ExchangeOutput(
            ExchangeRepository exchangeRepository
    ) {
        this.exchangeRepository = exchangeRepository;
    }

    @Override
    public void output(String value) {
        dataValueSequence.add(value);
    }

    @Override
    public void begin() {
        dataValueSequence = new ArrayList<>();
    }

    @Override
    public void finish() {
        try {
            LogManager.getLogger().debug(dataValueSequence);
            exchangeRepository.save(ExchangeBuilder
                    .getExchange(dataValueSequence)
            );

        } catch (Exception ex) {
            LogManager.getLogger().debug(ex);
        }
    }

}
