/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.exchange.yahoo;

import com.kay.clouds.domain.analytics.exchange.ExchangeService;
import com.kay.clouds.domain.analytics.exchange.IndexExchangeRepository;
import com.kay.clouds.domain.analytics.exchange.StockExchangeRepository;
import com.kay.clouds.persistence.analytics.DataWarehouseHelper;
import com.kay.clouds.persistence.analytics.exchange.index.CassandraIndexExchangeRepository;
import com.kay.clouds.persistence.analytics.exchange.stock.CassandraStockExchangeRepository;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.List;

public class MainRunner {

    public static void main(String[] args) throws IOException, InterruptedException {

        DataWarehouseHelper helper = DataWarehouseHelper
                .getCassandraDataSourceHelper("localhost");

        StockExchangeRepository stockRepository = new CassandraStockExchangeRepository(helper);
        IndexExchangeRepository indexRepository = new CassandraIndexExchangeRepository(helper);

        ExchangeService stockService = new ExchangeServiceImpl(
                stockRepository
        );

        ExchangeService indexService = new ExchangeServiceImpl(
                indexRepository
        );

        List<String> stockList = new ArrayList();
        stockList.add("ABE.MC");
        stockList.add("ANA.MC");
        stockList.add("ACX.MC");
        stockList.add("ACS.MC");
        stockList.add("AENA.MC");
        stockList.add("AMS.MC");
        stockList.add("MTS.MC");
        stockList.add("SAB.MC");
        stockList.add("POP.MC");
        stockList.add("BKIA.MC");
        stockList.add("BKT.MC");
        stockList.add("CABK.MC");
        stockList.add("DIA.MC");
        stockList.add("ENG.MC");
        stockList.add("ELE.MC");
        stockList.add("FCC.MC");
        stockList.add("FER.MC");
        stockList.add("GAM.MC");
        stockList.add("GAS.MC");
        stockList.add("GRF.MC");
        stockList.add("IDR.MC");
        stockList.add("MAP.MC");
        stockList.add("TL5.MC");
        stockList.add("MRL.MC");
        stockList.add("OHL.MC");
        stockList.add("REE.MC");
        stockList.add("REP.MC");
        stockList.add("SCYR.MC");
        stockList.add("TRE.MC");
        stockList.add("AI.PA");
        stockList.add("AIR.PA");
        stockList.add("ALV.DE");
        stockList.add("ITK.BE");
        stockList.add("ASML.AS");
        stockList.add("G.MI");
        stockList.add("CS.PA");
        stockList.add("BBVA.MC");
        stockList.add("SAN.MC");
        stockList.add("BAS.DE");
        stockList.add("BAYN.DE");
        stockList.add("BMW.DE");
        stockList.add("BNP.PA");
        stockList.add("CA.PA");
        stockList.add("SGO.PA");
        stockList.add("DAI.DE");
        stockList.add("BN.PA");
        stockList.add("DBK.DE");
        stockList.add("DPW.DE");
        stockList.add("DTE.DE");
        stockList.add("EOAN.DE");
        stockList.add("ENEL.MI");
        stockList.add("ENGI.PA");
        stockList.add("ENI.MI");
        stockList.add("EI.PA");
        stockList.add("FRE.DE");
        stockList.add("IBE.MC");
        stockList.add("ITX.MC");
        stockList.add("INGA.AS");
        stockList.add("ISP.MI");
        stockList.add("IAG.L");
        stockList.add("PHI1.DE");
        stockList.add("OR.PA");
        stockList.add("MC.PA");
        stockList.add("MUV2.D");
        stockList.add("NOKIA.HE");
        stockList.add("ORA.PA");
        stockList.add("SAF.PA");
        stockList.add("SAN.PA");
        stockList.add("SAP.DE");
        stockList.add("SU.PA");
        stockList.add("SIE.DE");
        stockList.add("GLE.PA");
        stockList.add("TEF.MC");
        stockList.add("FP.PA");
        stockList.add("UL.PA");
        stockList.add("UCG.MI");
        stockList.add("UNA.AS");
        stockList.add("DG.PA");
        stockList.add("VIV.PA");
        stockList.add("VOW3.DE");
        List<String> indexList = new ArrayList<>();
        indexList.add("%5E" + "IBEX");
        indexList.add("%5E" + "STOXX50E");

        indexService.scrapping(indexList);
        stockService.scrapping(stockList);
        exit(0);

    }
}
