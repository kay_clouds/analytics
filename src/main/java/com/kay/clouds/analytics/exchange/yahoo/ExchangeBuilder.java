package com.kay.clouds.analytics.exchange.yahoo;

import com.kay.clouds.domain.analytics.exchange.Exchange;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lili
 */
public class ExchangeBuilder {

    public static SimpleDateFormat YAHOO_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static Date parse(String value) throws ParseException {
        return YAHOO_DATE_FORMAT.parse(value);
    }

    public static Double toDouble(String value) {
        return Double.valueOf(value);
    }

    private static class ExchangeObject implements Exchange {

        private final List<String> values;

        public ExchangeObject(List<String> values) {

            if (values.size() < 8) {
                throw new IllegalArgumentException(
                        "List size is smarller than 8,"
                        + " cannot paerse to stock exchenge object"
                );
            }
            this.values = values;
        }

        @Override
        public String getTicker() {
            return values.get(0);
        }

        @Override
        public Date getDate() {

            try {
                return parse(values.get(1));
            } catch (ParseException ex) {
                return null;
            }

        }

        @Override
        public Double getOpenningPrice() {
            return toDouble(values.get(2));

        }

        @Override
        public Double getDailyMaxPrice() {
            return toDouble(values.get(3));
        }

        @Override
        public Double getDailyMinPrice() {
            return toDouble(values.get(4));
        }

        @Override
        public Double getAdjustedclosingPrice() {
            return toDouble(values.get(5));
        }

        @Override
        public Double getClosingPrice() {
            return toDouble(values.get(6));
        }

        @Override
        public Double getExchangeVolume() {
            return toDouble(values.get(7));
        }

    }

    public static Exchange getExchange(
            List<String> valueSequence
    ) {
        return new ExchangeObject(valueSequence);
    }

}
