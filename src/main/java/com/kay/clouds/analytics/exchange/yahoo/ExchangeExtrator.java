/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.exchange.yahoo;

import com.kay.clouds.analytics.crawler.Extractor;
import com.kay.clouds.analytics.crawler.Output;
import java.util.Arrays;
import org.jsoup.nodes.Document;

/**
 *
 * @author Lili
 */
public class ExchangeExtrator implements Extractor {

    private final String ticker;
    private final Output output;

    public ExchangeExtrator(String ticker, Output output) {

        //remove yahoo index '%5E'
        this.ticker = ticker.replace("%5E", "");
        this.output = output;
    }

    @Override
    public Boolean extract(Document doc) {

        String[] data = doc.body().text().split(" ");

        if (data.length <= 1) {
            return Boolean.FALSE;
        }

        Arrays.stream(data)
                .skip(1)
                .forEach(row -> {

                    output.begin();
                    output.output(ticker);
                    for (String element : row.split(",")) {
                        output.output(element);
                    }
                    output.finish();

                });

        return Boolean.TRUE;
    }

}
