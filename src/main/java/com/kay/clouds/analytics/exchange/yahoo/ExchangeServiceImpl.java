/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.exchange.yahoo;

import com.kay.clouds.analytics.crawler.Crawler;
import com.kay.clouds.analytics.crawler.Output;
import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.ExchangeRepository;
import com.kay.clouds.domain.analytics.exchange.ExchangeService;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lili
 */
public class ExchangeServiceImpl implements ExchangeService {

    private final ExchangeRepository<? extends Exchange> exchangeRepository;

    public ExchangeServiceImpl(
            ExchangeRepository<? extends Exchange> exchangeRepository
    ) {
        this.exchangeRepository = exchangeRepository;
    }

    private String getUrl(String ticker) {
        String url = "";

        List<? extends Exchange> exchangeList = exchangeRepository
                .getDescendantListBelow(ticker, new Date(), 1);

        if (exchangeList.isEmpty()) {
            url = UrlBuilder
                    .newAllHistoryUrlBuilder(ticker);
        } else {
            url = UrlBuilder
                    .newIndexUrlBuilder(ticker, exchangeList.get(0).getDate());

        }
        return url;
    }

    private void scrapping(
            String ticker,
            Output output,
            String url
    ) {
        Crawler crawler = new Crawler(
                new ExchangeExtrator(ticker, output)
        );

        crawler.crawl(url);

    }

    @Override
    public void scrapping(List<String> tickerList) {

        Output output = ExchangeOutput
                .newOutput(exchangeRepository);

        for (String ticker : tickerList) {

            String url = getUrl(ticker);
            scrapping(ticker, output, url);
        }

    }

}
