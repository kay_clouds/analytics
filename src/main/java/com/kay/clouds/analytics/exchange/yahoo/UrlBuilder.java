/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.exchange.yahoo;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.Date;

/**
 *
 * @author Lili He
 */
public class UrlBuilder {

    //^GSPC?period1=1492460179&period2=1495052179&interval=1d&events=history&crumb=dIqUKuRfma/
    private final static String URL_BASE = "https://query1.finance.yahoo.com/v7/finance/download/";

    private static String build(
            String stockName,
            Long dateInSeconds
    ) {
        LocalDateTime date = LocalDateTime.now();

        return URL_BASE
                + stockName + "?"
                + "&period1=" + dateInSeconds
                + "&period2=" + date.toEpochSecond(ZoneOffset.UTC)
                + "&interval=1d&events=history&crumb=dIqUKuRfma/";

    }

    public static String newAllHistoryUrlBuilder(String stockName) {
        //the yahoo history begin in 1993-02-15
        LocalDateTime date = LocalDateTime.of(1993, Month.FEBRUARY, 1, 1, 1);
        return UrlBuilder.build(stockName, date.toEpochSecond(ZoneOffset.UTC));
    }

    public static String newUrlBuilder(
            String stockName, LocalDateTime initialDate
    ) {

        return UrlBuilder.build(
                stockName,
                initialDate.toEpochSecond(ZoneOffset.UTC)
        );
    }

    public static String newIndexUrlBuilder(
            String ticker, Date initialDate
    ) {

        return UrlBuilder.build(
                ticker,
                initialDate.getTime() / 1000
        );
    }

}
