/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.web;

import com.kay.clouds.analytics.crawler.Output;
import com.kay.clouds.domain.analytics.web.WebMeta;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniel.eguia
 */
public class WebMetaOutput implements Output {

    private List<String> dataValueSequence;
    private WebMetaServiceImpl service;
    private WebMeta webMeta;
    private String pageUrl;

    public static WebMetaOutput newOutputStream(
            WebMetaServiceImpl service,
            String pageUrl
    ) {
        return new WebMetaOutput(service, pageUrl);
    }

    public WebMetaOutput(
            WebMetaServiceImpl service, String pageUrl
    ) {
        this.service = service;
        this.pageUrl = pageUrl;
    }

    @Override
    public void output(String value) {
        dataValueSequence.add(value);
    }

    @Override
    public void begin() {
        dataValueSequence = new ArrayList<>();
    }

    @Override
    public void finish() {
        try {
            webMeta = WebMetaDataBuilder.webmeta(dataValueSequence, pageUrl);

        } catch (Exception ex) {
        }
    }

    public WebMeta getWebMeta() {
        return webMeta;
    }

    public void setWebMeta(WebMeta webMeta) {
        this.webMeta = webMeta;
    }

}
