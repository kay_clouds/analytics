/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.web;

import java.io.IOException;
import static java.lang.System.exit;

public class MainRunner {

    public static void main(String[] args) throws IOException, InterruptedException {

        WebMetaServiceImpl service = new WebMetaServiceImpl() {
        };

        String url = "http://www.telecinco.es/";

        System.out.println(service.scrapping(url).toString());

        exit(0);

    }
}
