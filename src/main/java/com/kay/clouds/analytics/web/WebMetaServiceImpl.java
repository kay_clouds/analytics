/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.web;

import com.kay.clouds.analytics.crawler.Crawler;
import com.kay.clouds.analytics.crawler.Output;
import com.kay.clouds.domain.analytics.web.WebMeta;
import com.kay.clouds.domain.analytics.web.WebMetaService;

/**
 *
 * @author daniel.eguia
 */
public abstract class WebMetaServiceImpl implements WebMetaService {

    public WebMetaServiceImpl() {
    }

    private void scrapping(
            String urlBuilder, Output output
    ) {
        Crawler crawler = new Crawler(
                new WebMetaExtrator(output)
        );

        crawler.crawl(urlBuilder);

    }

    @Override
    public WebMeta scrapping(String url) {

        WebMetaOutput output = WebMetaOutput
                .newOutputStream(this, url);
        scrapping(url, output);
        return output.getWebMeta();

    }

}
