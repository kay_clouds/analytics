/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.web;

import com.kay.clouds.analytics.crawler.Extractor;
import com.kay.clouds.analytics.crawler.Output;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author daniel.eguia
 */
public class WebMetaExtrator implements Extractor {

    private final Output output;

    public WebMetaExtrator(Output output) {
        this.output = output;
    }

    private String getElement(Elements elements, String query, String atrr) {
        return elements.select(query).size() > 0 ? elements.select(query).get(0).attr(atrr) : "";
    }

    @Override
    public Boolean extract(Document doc) {

        Elements dataTable = doc.select("meta");
        output.begin();
        output.output(getElement(dataTable, "meta[name=title]", "content"));
        output.output(getElement(dataTable, "meta[name=description]", "content"));
        output.output(getElement(dataTable, "meta[name=keywords]", "content"));
        output.output(getElement(dataTable, "meta[name=image]", "content"));
        output.finish();
        return Boolean.TRUE;
    }

}
