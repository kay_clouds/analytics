package com.kay.clouds.analytics.web;

import com.kay.clouds.domain.analytics.web.WebMeta;
import java.util.List;

/**
 *
 * @author daniel.eguia
 */
public class WebMetaDataBuilder {

    public static class WebMetaObject implements WebMeta {

        private final List<String> valueSequence;
        private String pageUrl;

        private WebMetaObject(List<String> valueSequence, String pageUrl) {

            this.valueSequence = valueSequence;
            this.pageUrl = pageUrl;
        }

        @Override
        public String getName() {
            return valueSequence.get(0);
        }

        @Override
        public String getDescription() {
            return valueSequence.get(1);
        }

        @Override
        public String getKeywords() {
            return valueSequence.get(2);
        }

        @Override
        public String getImageUrl() {
            return valueSequence.get(3);
        }

        @Override
        public String getPageUrl() {
            return pageUrl;
        }

        @Override
        public String toString() {
            return "getName:" + getName() + ","
                    + "getDescription:" + getDescription() + ","
                    + "getKeywords:" + getKeywords() + ","
                    + "getImageUrl:" + getImageUrl() + ","
                    + "getPageUrl:" + getPageUrl();
        }

    }

    public static WebMetaObject webmeta(
            List<String> valueSequence, String pageUrl
    ) {
        return new WebMetaObject(valueSequence, pageUrl);
    }

}
