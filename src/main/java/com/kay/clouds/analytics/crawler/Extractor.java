/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.crawler;

import org.jsoup.nodes.Document;

/**
 *
 * @author Lili
 */
public interface Extractor {

    Boolean extract(Document doc);
}
