/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.crawler;

/**
 *
 * @author Lili
 */
public interface Output {

    void begin();

    void output(String value);

    void finish();

}
