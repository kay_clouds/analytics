/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.crawler;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author Lili
 */
public class Crawler {

    private final String USER_AGENT = "Chrome/7.0.517.41";
    private final Integer DEFUALT_TIMEOUT = 1000;
    private final String COOKIE_URL = "https://es.finance.yahoo.com/quote/ohl.mc?ltr=1";
    private final Extractor dataExtractor;
    private final Integer timeoutValue;
    private String yahooCookie;
    private String yqlCookie;

    public Crawler(Extractor dataExtractor) {
        this.dataExtractor = dataExtractor;
        timeoutValue = DEFUALT_TIMEOUT;
        yahooCookie = "t%3D%255EGSPC%252BYHOO";
        yqlCookie = "3ovklkpcgovk3&b=3&s=2o";
    }

    private Document execute(Integer timeoutValue, String url) throws IOException {
        return Jsoup.connect(url)
                .userAgent(USER_AGENT)
                .timeout(timeoutValue)
                .followRedirects(true)
                .cookie("PRF", yahooCookie)
                .cookie("B", yqlCookie)
                .execute()
                .parse();
    }

    private void renewCookie() throws IOException {
        Response response = Jsoup.connect(COOKIE_URL)
                .userAgent(USER_AGENT)
                .followRedirects(true)
                .timeout(timeoutValue)
                .execute();

        yahooCookie = response.cookie("PRF");
        yqlCookie = response.cookie("B");
    }

    private Boolean doGetData(String url) {
        Document doc;
        try {

            doc = execute(timeoutValue, url);
        } catch (Exception ex) {
            LogManager.getLogger().error(ex);
            try {
                renewCookie();
                doc = execute(timeoutValue * 2, url);
            } catch (Exception ex1) {
                return false;
            }
        }

        return dataExtractor.extract(doc);
    }

    public void crawl(String url) {

        LogManager.getLogger().error("get from: " + url);
        doGetData(url);

    }

}
