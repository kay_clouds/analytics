/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.quote.yahoo;

import com.kay.clouds.analytics.crawler.Crawler;
import com.kay.clouds.analytics.crawler.Output;
import com.kay.clouds.domain.analytics.quote.Quote;
import com.kay.clouds.domain.analytics.quote.QuoteService;

/**
 *
 * @author Lili
 */
public abstract class QuoteServiceImpl implements QuoteService {

    public QuoteServiceImpl() {

    }

    private void scrapping(
            String ticker,
            Output output,
            String url
    ) {
        Crawler crawler = new Crawler(
                new QuoteExtrator(output, ticker)
        );
        crawler.crawl(url);

    }

    @Override
    public void scrapping(String ticker) {

        Output output = QuoteOutput
                .newOutput(this);

        String url = UrlBuilder.build(ticker);
        scrapping(ticker, output, url);

    }

    abstract protected void save(Quote quote);

}
