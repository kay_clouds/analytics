/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.quote.yahoo;

import com.kay.clouds.analytics.crawler.Output;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lili
 */
public class QuoteOutput implements Output {

    private List<String> dataValueSequence;
    private QuoteServiceImpl service;

    public static Output newOutput(
            QuoteServiceImpl service
    ) {
        return new QuoteOutput(service);
    }

    public QuoteOutput(
            QuoteServiceImpl service
    ) {
        this.service = service;
    }

    @Override
    public void output(String value) {
        dataValueSequence.add(value);
    }

    @Override
    public void begin() {
        dataValueSequence = new ArrayList<>();
    }

    @Override
    public void finish() {
        try {
            service.save(QuoteBuilder
                    .quote(dataValueSequence)
            );

        } catch (Exception ex) {

        }
    }

}
