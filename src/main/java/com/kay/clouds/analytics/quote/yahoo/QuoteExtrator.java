/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.quote.yahoo;

import com.kay.clouds.analytics.crawler.Extractor;
import com.kay.clouds.analytics.crawler.Output;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author Lili
 */
public class QuoteExtrator implements Extractor {

    private final Output output;
    private final String ticker;

    public QuoteExtrator(Output output, String ticker) {
        this.output = output;
        this.ticker = ticker;
    }

    private String getElement(Elements elements, String query) {
        return elements.select(query).text().replaceAll(" ", "").replaceAll(",", "");
    }

    @Override
    public Boolean extract(Document doc) {

        Elements dataTable = doc.select("#quote-summary")
                .select("table tbody")
                .select("tr")
                .select("td[data-test]");

        String nowText = Long.toString(System.currentTimeMillis());

        output.begin();
        output.output(ticker);
        output.output(nowText);
        output.output(getElement(dataTable, "td[data-test=\"PREV_CLOSE-value\"]"));
        output.output(getElement(dataTable, "td[data-test=\"OPEN-value\"]"));
        output.output(getElement(dataTable, "td[data-test=\"BID-value\"]"));
        output.output(getElement(dataTable, "td[data-test=\"ASK-value\"]"));
        //outputStream.outputValue(getElement(dataTable, "td[data-test=\"DAYS_RANGE-value\"]"));
        output.output(getElement(dataTable, "td[data-test=\"TD_VOLUME-value\"]"));
        output.output(getElement(dataTable, "td[data-test=\"AVERAGE_VOLUME_3MONTH-value\"]"));
        output.finish();

        return Boolean.TRUE;
    }

}
