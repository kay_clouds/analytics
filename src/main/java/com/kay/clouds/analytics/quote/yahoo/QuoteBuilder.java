package com.kay.clouds.analytics.quote.yahoo;

import com.kay.clouds.domain.analytics.quote.Quote;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lili
 */
public class QuoteBuilder {

    private static class QuoteObject implements Quote {

        private final List<String> valueSequence;

        private QuoteObject(List<String> valueSequence) {
            this.valueSequence = valueSequence;
        }

        @Override
        public String getTicker() {
            return valueSequence.get(0);
        }

        @Override
        public Date getDate() {
            return new Date(Long.parseLong(valueSequence.get(1)));
        }

        private Double parseDouble(String text) {
            if (!text.isEmpty()) {
                return Double.parseDouble(text);
            }
            return 0.0;

        }

        private Integer parseInteger(String text) {
            if (!text.isEmpty()) {
                return Integer.parseInt(text);
            }
            return 0;
        }

        @Override
        public Double getPreviousClosePrice() {
            return parseDouble(valueSequence.get(2));
        }

        @Override
        public Double getOpenPrice() {
            return parseDouble(valueSequence.get(3));
        }

        @Override
        public Double getBidPrice() {
            return parseDouble(valueSequence.get(4).split("x")[0]);
        }

        @Override
        public Integer getBidVolume() {
            return parseInteger(valueSequence.get(4).split("x")[1]);
        }

        @Override
        public Double getAskPrice() {
            return parseDouble(valueSequence.get(5).split("x")[0]);
        }

        @Override
        public Integer getAskVolume() {
            return parseInteger(valueSequence.get(5).split("x")[1]);
        }

        @Override
        public Integer getVolume() {
            return parseInteger(valueSequence.get(6));
        }

        @Override
        public Integer getAverageVolume() {
            return parseInteger(valueSequence.get(7));
        }

        @Override
        public String toString() {
            return "getTicker:" + getTicker() + ","
                    + "getDate:" + getDate() + ","
                    + "getAskPrice:" + getAskPrice() + ","
                    + "getAskVolume:" + getAskVolume() + ","
                    + "getAverageVolume:" + getAverageVolume() + ","
                    + "getBidPrice:" + getBidPrice() + ","
                    + "getBidVolume:" + getBidVolume() + ","
                    + "getOpenPrice:" + getOpenPrice() + ","
                    + "getPreviousClosePrice:" + getPreviousClosePrice() + ","
                    + "getVolume:" + getVolume();
        }

    }

    public static QuoteObject quote(List<String> valueSequence) {
        return new QuoteObject(valueSequence);
    }

}
