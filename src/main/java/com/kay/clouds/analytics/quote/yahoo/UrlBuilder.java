/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.quote.yahoo;

/**
 *
 * @author Lili He
 */
public class UrlBuilder {

    private final static String urlBase = "https://finance.yahoo.com/quote/";

    public static String build(String tickerName) {
        return urlBase + tickerName;

    }

}
