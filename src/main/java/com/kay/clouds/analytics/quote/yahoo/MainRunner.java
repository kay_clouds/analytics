/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.analytics.quote.yahoo;

import com.kay.clouds.domain.analytics.quote.Quote;
import java.io.IOException;
import static java.lang.System.exit;

public class MainRunner {

    public static void main(String[] args) throws IOException, InterruptedException {

        QuoteServiceImpl service = new QuoteServiceImpl() {
            @Override
            protected void save(Quote quote) {
                System.out.println(quote.toString());
            }
        };

        service.scrapping("SCYR.MC");

        exit(0);

    }
}
